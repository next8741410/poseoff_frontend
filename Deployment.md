# GITLAB Repos:
```
https://gitlab.com/next8741410/poseoff_backend
https://gitlab.com/next8741410/poseoff_inference
https://gitlab.com/next8741410/poseoff_frontend
```

# Kubernetes deployment:

# Below command will Create the namespace  poseoff-inference
```
kubectl create ns poseoff-inference
```

```
kubectl get sc ##to get list of StorageClasses
``` 
```
git clone https://gitlab.com/next8741410/poseoff_backend.git
cd poseoff_backend/kube_poseoff_backend
```
# Update selected  storageClassName in mysqlpvc.yaml and pv.yaml file Manually
# Below commands will create  PersistentVolumeClaim and PersistentVolume object 

```

kubectl apply -f pv.yaml -n poseoff-inference

kubectl apply -f mysqlpvc.yaml -n poseoff-inference

#once PVC is created Check the status of PVC and PV with below Commands 

Kubectl get pv -n poseoff-inference

kubectl get pvc -n poseoff-inference

# apply mysql deployment by below command
kubectl apply -f mysql.yaml  -n poseoff-inference 
```
## Note: if you change password in “mysql.yaml” before previous command than you need to update DB_URL as well with new password in configmapbackedn.yaml   (before next command)

## Deployment of poseoff_backend
```

kubectl apply -f configmapbackedn.yaml  -n poseoff-inference
```
```
kubectl apply -f testbackend.yaml -n poseoff-inference
kubectl apply -f  backend-service.yaml -n poseoff-inference
```
## Deployment of poseoff_inference at Edge

cd ~

git clone https://gitlab.com/next8741410/poseoff_inference.git

cd poseoff_inference
```
kubectl apply -f kube_inference.yaml -n poseoff-inference
kubectl apply -f inference-service.yaml -n poseoff-inference
```
## Deployment of poseoff_inference at Cloud
connect to cloud cluster and execute below commands 

cd ~

git clone https://gitlab.com/next8741410/poseoff_inference.git

cd poseoff_inference
```
kubectl apply -f kube_inference.yaml -n poseoff-inference
kubectl apply -f inference-service.yaml -n poseoff-inference
```

```
# for getting the URL of  poseoff_backend and poseoff_inference on edge run Below command
kubectl get svc -n poseoff-inference 
```
## Note: before running next command, please update below URLs in “frontconfig.yaml”

    • CLOUD_URL: poseoff-inference-service on cloud deployment URL
    • EDGE_URL:poseoff-inference-service  on EDGE deployment URL
    • BACKEND_URL: poseoff-backend-service  deployment URL
    • PRICYTRAY_URL:  Price a tray deployment URL

```
    Ex:below is example of updated frontconfig.yaml
    • CLOUD_URL: "https://poseoff-edge-4yerohykja-uc.a.run.app" (Poseoff_inference on cloud deployment URL)
    • EDGE_URL: "http://34.122.205.27:9002" (Poseoff_inference on EDGE deployment URL)
    • BACKEND_URL: "http://34.170.172.220:8082" (Poseoff_backend deployment URL)
    • PRICYTRAY_URL: "https://example.com" (Price a tray deployment URL)
 ```

## Deployment of poseoff_frontend

cd ~

git clone https://gitlab.com/next8741410/poseoff_frontend.git

cd poseoff_frontend/kube_poseoff_frontend

```
kubectl apply -f frontconfig.yaml -n poseoff-inference
```
```
kubectl apply -f poseoffrontend.yaml -n poseoff-inference

 kubectl apply -f frontend-service.yaml  -n poseoff-inference

```
## Run below  command to get frontend/website url
```
kubectl get svc -n poseoff-inference  
```

#How to run 

1. Select the game mode and configure camera:
    On the home screen click on the setting icon to select game mode and configure active camera eg: webcam, usbcam, rtsp-cam (for rtsp-cam provide rtsp camera url in text box) than click on save setting. Please note if the RTSP url is incorrect user will face black screen in the game and will fail all poses.
2. Start the game
    On the home screen click on Pose game to start

## NOTE:
webcam camera is only working with https(ssl)
    

