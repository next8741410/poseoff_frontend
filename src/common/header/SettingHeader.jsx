import React from "react";
import btnClose from "../../assets/btn-close.png";
import PoseOff from "../../assets/thumb-TT-selected.png";
import PoseTxt from "../../assets/Pose-Off.png";
import Logo from "../../assets/Logo.png";

export default function SettingHeader({handleClose}) {

  return(
    <div className="cloud-hdr">
        <div className="itemContainerHeader">
          <div>
            <div className="select-text" style={{visibility:"hidden"}}>Settings</div>
            <div className="service-textSH">Settings</div>
          </div>
          <div className="close-btnSH">
            <img src={btnClose} onClick={handleClose}></img>
          </div>
        </div>
        <div className="tidy-tPSH">
          <img src={Logo}></img>
        </div>
      </div>
  )
}