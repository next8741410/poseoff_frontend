import React from "react";
import btnClose from "../../assets/btn-close.png";
import PoseOff from "../../assets/thumb-TT-selected.png";
import PoseTxt from "../../assets/Pose-Off.png";
export default function ServiceHeader({handleClose}) {

  return(
    <div className="cloud-hdr">
        <div className="itemContainerHeader">
          <div>
            <div className="select-text" style={{visibility:"hidden"}}>Select</div>
            <div className="service-text">Test your flexibility: strike a pose or laugh trying!</div>
          </div>
          <div className="close-btn">
            <img src={btnClose} onClick={handleClose}></img>
          </div>
        </div>
        <div className="tidy-tPSS">
          <img src={PoseOff}></img>
          <div className="tidy-tPS-posetxtss">
            <img src={PoseTxt}></img>
          </div>
        </div>
      </div>
  )
}