import React, { useEffect, useRef, useState } from "react"

export default function ProgressLoader({time=10, onFinish=()=>{}, bgColor="#5e98f7", fgColor="#0B57D0"}){

  const [width, setWidth] = useState(0);
  const curTime = useRef(0);
  const interval = useRef(null);
  useEffect(()=>{
    updateWidth();

    return ()=>{
      if(interval.current!=null){
        clearInterval(interval.current)
      }
    }
  }, []);
  function updateWidth(){

    interval.current = setInterval(()=>{
      curTime.current += 60/1000;
      let newWidth = (100/time)*curTime.current;
      setWidth(newWidth);
      if(curTime.current>=time){
        clearInterval(interval.current);
        setWidth(100);
        onFinish();
      } 
    }, 60)
   
    
    
  }
  return(
    <div className="progress_loader_box" style={{backgroundColor:bgColor}}>
      <div style={{width:`${width}%`, backgroundColor:fgColor}}></div>
    </div>
  )
}