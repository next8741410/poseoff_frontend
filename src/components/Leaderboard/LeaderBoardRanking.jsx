import React, { useEffect, useState, useRef, useContext } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "./leaderBoard.css";
import UrgeWithPleasureComponent from "../TimerComponent/Timer";
import {
  postApiGcp,
  getApi,
  getApiUrl2,
} from "../../service/service/Service";
import { apiPaths } from "../../service/apipaths/ApiPaths";
import { mainContext } from "../../store/Store";
import btnClose from "../../assets/btn-close.png";
import reflection from "../../assets/reflection.png";
import userBlack from "../../assets/user-black.png";
import userWhite from "../../assets/user-white.png";

const Leaderboard = () => {
  const navigate = useNavigate();
  const { userInfo, spinnerInfo } = useContext(mainContext);
  const [rankData, setRankData] = useState([]);
  const [currentUserData, setCurrentUserData] = useState(null);

  useEffect(() => {
    getRankData();
}, []);

const handleClose = () => {
  navigate('/EventPhoto')
}

function getRankData() {
  if (!userInfo.txtId || userInfo.txtId == undefined) {
    getApiUrl2(apiPaths.getUsersRank + "?tx_id=9999")
      .then((res) => {
        if (Array.isArray(res.data)) {
          setRankData(res.data);
          let currUser = res.data.find(item => item.current_user_rank);
          setCurrentUserData(currUser);
        }
      })
      .catch((err) => {

      })
  }
  else {
    getApiUrl2(apiPaths.getUsersRank + "?tx_id=" + userInfo.txtId)
      .then((res) => {
        if (Array.isArray(res.data)) {
          setRankData(res.data);
          let currUser = res.data.find(item => item.current_user_rank);
          setCurrentUserData(currUser);
        }
      })
      .catch((err) => {

      })
  }
}
const [leaderBoardData, seLeaderBoardData] = useState([])

  return (
    <div className="leader_board_container">
      <div className="laader_baord_inner_container">
        <div className="leader_board_head">
          <div className="leader_board_body">
            <div>
              Leaderboard
            </div>
            <div className="close_btn" onClick={handleClose}>
              <img src={btnClose} />
            </div>
          </div>
        </div>
        <div className="leader_ranking_contaner">
          {rankData.slice(0,10).map(((item, index)=>{
           
            let textColor ="#FFFFFF";
            let timeColor = "#FFFFFF";
            let rankBgColor = "#FFFFFF";
            let rankTextColor="#0B57D0";
            let color = "blue";
            if(index<3){
              rankBgColor="#000000";
              timeColor="#00672B"; 
              textColor="#000000";
              rankTextColor="#FFFFFF";
              color="yellow";
            } 
            return(
              <div style={{margin:"4px 0px"}}>
                <LeaderRow data={item} color={color} textColor={textColor} timeColor={timeColor} rankBgColor={rankBgColor} rankTextColor={rankTextColor}/>
              </div>
            )
          }))}
        </div>
        {/* <div style={{marginTop:"28px"}}></div> */}
        <div className="leader_board_bottom">
          <div className="leader_board_bottom_body">
            {currentUserData!=null && 
              <LeaderRow data={currentUserData} color="dark_blue" textColor="#FFFFFF" timeColor="#FFFFFF" rankBgColor="#65FFB2" rankTextColor="#000000"/>
            }
          </div>
        </div>
      </div>
      
    </div>
  );
};

// {data.slice(0, 10).map(item => (
//   <li key={item.id}>{item.name}</li>
// ))}


function LeaderRow({
  data, 
  color, 
  rankBgColor="#000000", 
  timeColor="#00672B", 
  textColor="#000000",
  rankTextColor="#FFFFFF"
}){

  let poseArray =[
    {name:"Pose 1", score:data.pose_1},
    {name:"Pose 2", score:data.pose_2},
    {name:"Pose 3", score:data.pose_3},
    {name:"Pose 4", score:data.pose_4},
    {name:"Pose 5", score:data.pose_5},
    {name:"Pose 6", score:data.pose_6},
    {name:"Pose 7", score:data.pose_7},
    {name:"Pose 8", score:data.pose_8},
    {name:"Pose 9", score:data.pose_9},
    {name:"Pose 10", score:data.pose_10},
  ];
  console.log(data);
  let background = 'linear-gradient(95deg,#F6DAAF 0%,#FEE755 100%)';
  let imgSrc = userBlack;
  if(color=="blue"){
    background = 'linear-gradient(95deg,#337CF1 0%,#053A8F 100%)'
    imgSrc =userWhite;
  } else if(color=='dark_blue'){
    background = '#02346E'
    imgSrc =userWhite;
  }
  return(
    <div className="leader_row_container" style={{background:background}}>
      <div className="leader_row_box">
        <div className="leader_row_img_box">
          <img src={imgSrc}/>
        </div>
        <div className="leader_row_name_box" style={{color:textColor}}>
          <div>{data.username}</div>
          {/* <div>email@email.com</div> */}
        </div>
        <div className="leader_row_ranking_box">
          {poseArray.map(item=>{
            let innerBgColor = "#2ED573";
            if(item.score==""){
              innerBgColor = "#EC4058"
            }
            return(
            <div className="leader_row_rank_box" style={{color:textColor}}>
              <div  style={{backgroundColor:innerBgColor}}>{item.score!=""?`${item.score}s`:""}</div>
              <div>{item.name}</div>
            </div>
            )
          })}
        
        </div>
        <div className="leader_row_time_box" >
          <div style={{color:timeColor}}>{data.final_score}s</div>
          <div style={{color:textColor}}>on {data.platform=="EDGE"?"edge":"cloud"}</div>
        </div>
        <div className="leader_row_curr_rank_box">
          <div style={{color:rankTextColor, backgroundColor:rankBgColor}}>{data.rank}</div>
        </div>
      </div>
      <div className="leader_box_reflection_img">
        <img src={reflection}/>
      </div>
    </div>
  )
}
export default Leaderboard;
