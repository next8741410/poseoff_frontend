import React, { useState } from 'react';
import "./carousel.css"

const ImageSlider = ({ images }) => {
  const [currentImage, setCurrentImage] = useState(0);

  const nextImage = () => {
    setCurrentImage((prevImage) => (prevImage + 1) % images.length);
  };

  const prevImage = () => {
    setCurrentImage((prevImage) =>
      prevImage === 0 ? images.length - 1 : prevImage - 1
    );
  };

  setInterval(nextImage,2400);

  return (
    <div className="image-slider">
      {/* <button onClick={prevImage}>&lt;</button> */}
      <img src={images[currentImage]} alt={`Slide ${currentImage + 1}`} />
      {/* <button onClick={nextImage}>&gt;</button> */}
    </div>
  );
};

export default ImageSlider;