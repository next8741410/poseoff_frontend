import React, { useContext, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./platformselect.css";
import { mainContext } from "../../store/Store";
import PlatformHeader from "../../common/header/PlatformHeader";
import ProgressLoader from "../../common/ProgressLoader";
import Wheeler from "../WheelSpinner/Wheeler";

const PlatformSelectPage = () => {
  const navigate = useNavigate();
  const { setSpinnerInfo } = useContext(mainContext);
  const fortuneWheelText = useRef(null);
  const handleClose = () => {
    navigate("/EventPhoto");
  }

  const selectedService = () => {
    if(fortuneWheelText.current==null){
      setSpinnerInfo("EDGE");
    }
    navigate('/SelectedService');
  }
  const onFinished = (winner) => {
    setSpinnerInfo(winner);
    fortuneWheelText.current = winner;
  };
  const segments = [
    "EDGE",
    "EDGE",
    "CLOUD",  
    "EDGE",
    "EDGE",
    "CLOUD",
    "EDGE",
    "EDGE",
    "CLOUD",
    "EDGE",
    "EDGE",
    "CLOUD",
  ];
  const segColors = [
    "#cd4548",
    "#1691d4",
    "#62b48c",
    "#ffa20f",
    "#7b6bb7",
    "#909a8c",
    "#7a1f1f",
    "#d1a365",
    "#114a96",
    "#cd4548",
    "#1691d4",
    "#62b48c",
    "#ffa20f",
    "#7b6bb7",
    "#909a8c",
    "#7a1f1f",
    "#d1a365",
    "#114a96"
  ];
  return (
    <>
      <PlatformHeader handleClose={handleClose}/>
      <div className="cloud-wrapper">
        <div className="wheelBox">
        <Wheeler
          texts={segments}
          colors={segColors}
          onFinish={(winner) => onFinished(winner)}
          parts={12}
          style={{fontWeight: "bold",fontSize: "25px"}}
        />
      </div>
      </div>
      <div className="tapClass">
        Tap the fortune wheel to select platform
      </div>
      <div className="leader__board-button-containerPS">
        <div className="" onClick={selectedService}>
          <div className="loader_boxPS">
            <ProgressLoader time={10} onFinish={selectedService}/>
          </div>
        </div>
      </div>
      <div className="bottomFooter">
        <p id="bottomMarkText">
          Powered by <b>Google</b>
        </p>
      </div>
    </>
  );
};

export default PlatformSelectPage;
